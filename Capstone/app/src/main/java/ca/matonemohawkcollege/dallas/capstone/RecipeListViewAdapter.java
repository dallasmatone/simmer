package ca.matonemohawkcollege.dallas.capstone;

import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_NAME_COLUMN;
import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_DESCRIPTION_COLUMN;
import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_RATING_COLUMN;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Dallas on 1/1/2018.
 */

public class RecipeListViewAdapter extends BaseAdapter {

    public ArrayList<HashMap<String, String>> RecipeList;
    Activity activity;
    TextView txtRecipeName;
    TextView txtRecipeDescription;
    TextView txtRecipeRating;

    public RecipeListViewAdapter(Activity activity,ArrayList<HashMap<String, String>> list){
        super();
        this.activity = activity;
        this.RecipeList = list;
    }

    @Override
    public int getCount() {
        return RecipeList.size();
    }

    @Override
    public Object getItem(int position) {
        return RecipeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=activity.getLayoutInflater();

        if(convertView == null){
            convertView = inflater.inflate(R.layout.multilined_list_view_adapter, null);

            txtRecipeName = (TextView) convertView.findViewById(R.id.lbl_recipeName);
            txtRecipeDescription = (TextView) convertView.findViewById(R.id.txt_recipeDescription);
            txtRecipeRating = (TextView) convertView.findViewById(R.id.txt_recipeRating);
        }

        HashMap<String, String> map = RecipeList.get(position);
        txtRecipeName.setText(map.get(RECIPE_NAME_COLUMN));
        txtRecipeDescription.setText(map.get(RECIPE_DESCRIPTION_COLUMN));
        txtRecipeRating.setText(map.get(RECIPE_RATING_COLUMN));

        return convertView;
    }
}
