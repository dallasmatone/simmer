package ca.matonemohawkcollege.dallas.capstone.Views;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import at.markushi.ui.CircleButton;
import ca.matonemohawkcollege.dallas.capstone.R;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileScreen extends Fragment {

    private CircleButton userProfileBtn;
    private ImageButton editBtn;
    private TwitterLoginButton twitterBtn;
    private ImageView profilePictureImg;
    private TextView userNameText;
    private EditText descriptionText;
    private Boolean isEditMode = false;

    public UserProfileScreen() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        getActivity().setTitle("Your Profile");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Twitter.initialize(getActivity());
        View view = inflater.inflate(R.layout.fragment_user_profile_screen, container, false);
        userProfileBtn = (CircleButton) view.findViewById(R.id.userProfilePicBtn);
        editBtn = (ImageButton) view.findViewById(R.id.editBtn);
        twitterBtn = (TwitterLoginButton) view.findViewById(R.id.twitterBtn);
        profilePictureImg = (ImageView) view.findViewById(R.id.profilePictureImg);
        descriptionText = (EditText) view.findViewById(R.id.descriptionText);
        userNameText = (TextView) view.findViewById(R.id.profileNamelbl);

        OnLoad();

        userProfileBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                alertDialog();
            }
        });

        editBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(!isEditMode) {
                    editBtn.setImageResource(R.drawable.icons8_save);
                    isEditMode = true;
                }
                else{
                    isEditMode = false;
                    editBtn.setImageResource(R.drawable.icons8_edit);
                }
                descriptionText.setFocusableInTouchMode(isEditMode);
                descriptionText.setFocusable(isEditMode);
                userProfileBtn.setEnabled(isEditMode);
                //facebookBtn.setEnabled(isEditMode);
                twitterBtn.setEnabled(isEditMode);
                //instagramBtn.setEnabled(isEditMode);
            }
        });

        twitterBtn.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(getActivity(), "Twitter Authentication Failed", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case 0:
                if(resultCode == RESULT_OK){
                    Bitmap bitmap = (Bitmap)data.getExtras().get("data");
                    profilePictureImg.setImageBitmap(bitmap);
                }
                break;
            case 1:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = data.getData();
                    profilePictureImg.setImageURI(selectedImage);
                }
                break;
            case 2:
                // Pass the activity result to the login button.
                twitterBtn.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void OnLoad(){
        userNameText.setText(getArguments().getString("UserName"));

        userProfileBtn.setEnabled(isEditMode);
        twitterBtn.setEnabled(isEditMode);
        descriptionText.setFocusable(isEditMode);
    }

    private void alertDialog(){
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent,0);
                } else if (items[item].equals("Choose from Library")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);//one can be replaced with any action code
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

}
