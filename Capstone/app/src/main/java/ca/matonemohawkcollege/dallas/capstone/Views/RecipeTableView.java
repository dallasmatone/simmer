package ca.matonemohawkcollege.dallas.capstone.Views;

import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_NAME_COLUMN;
import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_DESCRIPTION_COLUMN;
import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_RATING_COLUMN;
import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_ID;
import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_IS_FAVOURITE;
import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.USER_ID;
import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.ID;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.SearchView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ca.matonemohawkcollege.dallas.capstone.Models.Recipes;
import ca.matonemohawkcollege.dallas.capstone.NonScrollListView;
import ca.matonemohawkcollege.dallas.capstone.R;
import ca.matonemohawkcollege.dallas.capstone.RecipeListViewAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeTableView extends Fragment {

    private ArrayList<HashMap<String, String>> _recipeList;
    private NonScrollListView _recipeListView;
    private ImageButton _filterRecipes;
    private RecipeListViewAdapter adapter;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.recipe_search, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                ArrayList<HashMap<String, String>> tempList = new ArrayList<>();

                for (HashMap<String, String> temp : _recipeList){
                    if(temp.get(RECIPE_NAME_COLUMN).toLowerCase().contains(s.toLowerCase())){
                        tempList.add(temp);
                    }
                }

                adapter = new RecipeListViewAdapter(getActivity(), tempList);
                _recipeListView.setAdapter(adapter);

                return true;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recipe_table_view, container, false);
        setHasOptionsMenu(true);

        _recipeListView = (NonScrollListView)view.findViewById(R.id.lv_RecipeList);
        _filterRecipes = (ImageButton) view.findViewById(R.id.btn_FilterTags);

        _recipeList = new ArrayList<HashMap<String,String>>();
        OnLoad();

        adapter = new RecipeListViewAdapter(getActivity(), _recipeList);
        _recipeListView.setAdapter(adapter);

        _recipeListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id)
            {
                Animation animation1 = new AlphaAnimation(0.3f, 1.0f);
                animation1.setDuration(1000);
                view.startAnimation(animation1);

                HashMap<String,String> item = (HashMap<String,String>) adapter.getItem(position);
                Fragment fragment = new RecipeScreen();
                Bundle bundle = new Bundle();
                bundle.putString("recipeName", item.get(RECIPE_NAME_COLUMN));
                bundle.putString("recipeDescription", item.get(RECIPE_DESCRIPTION_COLUMN));
                bundle.putString("recipeRating", item.get(RECIPE_RATING_COLUMN));
                bundle.putString("ID", item.get(ID));
                bundle.putString("recipeFav", item.get(RECIPE_IS_FAVOURITE));
                bundle.putString("UserID", item.get(USER_ID));
                bundle.putString("recipeID", item.get(RECIPE_ID));
                bundle.putString("CRUD", "View");
                fragment.setArguments(bundle);

                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.content_container, fragment);
                fragmentTransaction.addToBackStack("RecipeSearch");
                fragmentTransaction.commit();
                setHasOptionsMenu(false);
            }

        });

        _filterRecipes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
            }
        });

        return view;
    }

    private void OnLoad(){
        NavBar nav = (NavBar)getActivity();
        for(Recipes recipe : nav.RecipeList){
            HashMap<String,String> temp = new HashMap<String, String>();
            temp.put(RECIPE_NAME_COLUMN, recipe.getRecipeName());
            temp.put(RECIPE_DESCRIPTION_COLUMN, recipe.getRecipeDescription());
            temp.put(RECIPE_RATING_COLUMN, String.valueOf(recipe.getRecipeRating()));
            temp.put(RECIPE_ID, recipe.getRecipeID());
            temp.put(RECIPE_IS_FAVOURITE, String.valueOf(recipe.getIsFavorutie()));
            temp.put(USER_ID, String.valueOf(recipe.user_ID));
            temp.put(ID, recipe.getId());
            _recipeList.add(temp);
        }

    }

}
