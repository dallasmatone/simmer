package ca.matonemohawkcollege.dallas.capstone.Views;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabWidget;

import ca.matonemohawkcollege.dallas.capstone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeSearchScreen extends Fragment {

    //Declare Public Variables
    private FragmentTabHost mTabHost;

    public RecipeSearchScreen() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        getActivity().setTitle("Recipe Book");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mTabHost = new FragmentTabHost(getActivity());
        mTabHost.setup(getActivity(), getChildFragmentManager(), R.layout.fragment_recipe_search_screen);

        //Tab 1
        InitalizeBreakfastsTab();
        //Tab 2
        InitalizeLunchsTab();
        //Tab 3
        InitalizeDinnersTab();
        //Tab 4
        InitalizeDesetsTab();
        //Tab 5
        InitalizeAppietizersTab();
        //Tab 6
        InitalizeOtherTab();

        for(int i = 0; i < 6; i++)
            mTabHost.getTabWidget().getChildAt(i).setBackgroundColor(getResources().getColor(R.color.colorMain));

        View tabs = (TabWidget) mTabHost.findViewById(android.R.id.tabs);
        ViewGroup parent = (ViewGroup) mTabHost.getChildAt(0);
        parent.removeView(tabs);
        parent.addView(tabs);

        return mTabHost;
    }

    private void InitalizeBreakfastsTab(){
        TabHost.TabSpec spec = mTabHost.newTabSpec("Tab One");
        spec.setIndicator("",getResources().getDrawable(R.drawable.icons8_pancake));
        mTabHost.addTab(spec, RecipeTableView.class, null);
    }

    private void InitalizeLunchsTab(){
        TabHost.TabSpec spec = mTabHost.newTabSpec("Tab Two");
        spec.setIndicator("",getResources().getDrawable(R.drawable.icons8_sandwich));
        mTabHost.addTab(spec, RecipeTableView.class, null);
    }

    private void InitalizeDinnersTab(){
        TabHost.TabSpec spec = mTabHost.newTabSpec("Tab Three");
        spec.setIndicator("",getResources().getDrawable(R.drawable.icons8_barbecue));
        mTabHost.addTab(spec, RecipeTableView.class, null);
    }

    private void InitalizeDesetsTab(){
        TabHost.TabSpec spec = mTabHost.newTabSpec("Tab Four");
        spec.setIndicator("",getResources().getDrawable(R.drawable.icons8_cupcake));
        mTabHost.addTab(spec, RecipeTableView.class, null);
    }

    private void InitalizeAppietizersTab(){
        TabHost.TabSpec spec = mTabHost.newTabSpec("Tab Five");
        spec.setIndicator("",getResources().getDrawable(R.drawable.icons8_nachos));
        mTabHost.addTab(spec, RecipeTableView.class, null);
    }

    private void InitalizeOtherTab(){
        TabHost.TabSpec spec = mTabHost.newTabSpec("Tab Six");
        spec.setIndicator("",getResources().getDrawable(R.drawable.icons8_other));
        mTabHost.addTab(spec, RecipeTableView.class, null);
    }
}
