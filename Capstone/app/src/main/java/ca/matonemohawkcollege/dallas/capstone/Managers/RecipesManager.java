package ca.matonemohawkcollege.dallas.capstone.Managers;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.OkHttpClientFactory;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.squareup.okhttp.OkHttpClient;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import ca.matonemohawkcollege.dallas.capstone.Base.UserBase;
import ca.matonemohawkcollege.dallas.capstone.Models.Ingredients;
import ca.matonemohawkcollege.dallas.capstone.Models.Recipes;
import ca.matonemohawkcollege.dallas.capstone.Models.Users;
import ca.matonemohawkcollege.dallas.capstone.R;
import ca.matonemohawkcollege.dallas.capstone.Views.LoginScreen;
import ca.matonemohawkcollege.dallas.capstone.Views.NavBar;

/**
 * Created by Dallas on 1/5/2018.
 */

public class RecipesManager extends Activity {
    private Activity myActivity;
    private Recipes _recipe;
    private List<Recipes> _recipeList;

    /**
     * Mobile Service Client reference
     */
    private MobileServiceClient mClient;

    /**
     * Mobile Service Table used to access data
     */
    private MobileServiceTable<Recipes> mRecipesTable;

    public RecipesManager(Activity inActivity) {
        myActivity = inActivity;
        _recipeList = new ArrayList<>();
        _recipe = new Recipes();

        try {
            // Create the Mobile Service Client instance, using the provided

            // Mobile Service URL and key
            mClient = new MobileServiceClient(
                    "https://simmer.azurewebsites.net/",
                    myActivity);

            // Extend timeout from default of 10s to 20s
            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
                @Override
                public OkHttpClient createOkHttpClient() {
                    OkHttpClient client = new OkHttpClient();
                    client.setReadTimeout(20, TimeUnit.SECONDS);
                    client.setWriteTimeout(20, TimeUnit.SECONDS);
                    return client;
                }
            });

            // Get the Mobile Service Table instance to use

            mRecipesTable = mClient.getTable("Recipes",Recipes.class);


        } catch (MalformedURLException e) {

        } catch (Exception e) {

        }

    }

    public void GetUsersRecipes(String userId){
        _recipe.setUserID(userId);
        GetRecipes(_recipe);
    }

    public void CreateNewRecipe(String userID, String recipeName) {

        try {
            _recipe = new Recipes();
            _recipe.setId(UUID.randomUUID().toString());
            _recipe.setRecipeID(UUID.randomUUID().toString());
            _recipe.setUserID(userID);
            _recipe.setRecipeName(recipeName);
            _recipe.setRecipeCreator(userID);
            CreateRecipe(_recipe);
        } catch (Exception e){

        }
    }

    public void SaveRecipe(Recipes recipe) {
        _recipe = recipe;
        Save(_recipe);
    }

    private void GetRecipes(final Recipes item) {

        new AsyncTask<String, Void, String>(){
            @Override
            protected String doInBackground(String... params) {
                String test = "";
                try {
                    _recipeList = refreshItemsFromMobileServiceTable(item);

                } catch (final Exception e){
                    System.out.println(e);
                }

                return test;
            }
            @Override
            protected void onPostExecute(String result) {
                ((NavBar) myActivity).viewRecipes(_recipeList);
            }
        }.execute();
    }

    private void CreateRecipe(Recipes recipe) {
        if (mClient == null) {
            return;
        }

        final Recipes item = recipe;

        // Insert the new item
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final Recipes entity = addItemInTable(item);
                } catch (final Exception e) {
                    System.out.println(e);
                }
                return null;
            }
        };

        runAsyncTask(task);
        ((NavBar) myActivity).createNewRecipeScreen(item);
    }

    public void Save(final Recipes item) {
        if (mClient == null) {
            return;
        }

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    checkItemInTable(item);
                } catch (final Exception e) {
                    System.out.println(e);
                }

                return null;
            }
        };

        runAsyncTask(task);
    }

    /**
     * Mark an item as completed in the Mobile Service Table
     *
     * @param item
     *            The item to mark
     */
    public void checkItemInTable(Recipes item) throws ExecutionException, InterruptedException {
        mRecipesTable.update(item).get();
    }

    private AsyncTask<Void, Void, Void> runAsyncTask(AsyncTask<Void, Void, Void> task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            return task.execute();
        }
    }

    public Recipes addItemInTable(Recipes item) throws ExecutionException, InterruptedException {
        Recipes entity = mRecipesTable.insert(item).get();
        return entity;
    }


    private List<Recipes> refreshItemsFromMobileServiceTable(Recipes item) throws ExecutionException, InterruptedException {
        return  mRecipesTable
                .where()
                .field("user_ID").eq(item.user_ID)
                .execute().get();
    }

}
