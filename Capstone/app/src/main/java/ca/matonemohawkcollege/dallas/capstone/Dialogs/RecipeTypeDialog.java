package ca.matonemohawkcollege.dallas.capstone.Dialogs;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import ca.matonemohawkcollege.dallas.capstone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeTypeDialog extends DialogFragment {

    private RadioButton _breakfast;
    private RadioButton _lunch;
    private RadioButton _dinner;
    private RadioButton _desert;
    private RadioButton _appitizer;
    private RadioButton _other;
    private RadioGroup _radioGroup;
    private Button _saveBtn;
    private Button _cancelBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_recipe_type_dialog, null);

        _radioGroup = (RadioGroup) view.findViewById(R.id.rg_RecipeType);
        _breakfast = (RadioButton) view.findViewById(R.id.rb_TypeBreakfast);
        _lunch = (RadioButton) view.findViewById(R.id.rb_TypeLunch);
        _dinner = (RadioButton) view.findViewById(R.id.rb_TypeDinner);
        _desert = (RadioButton) view.findViewById(R.id.rb_TypeDesert);
        _appitizer = (RadioButton) view.findViewById(R.id.rb_TypeAppetizer);
        _other = (RadioButton) view.findViewById(R.id.rb_TypeOther);
        _saveBtn = (Button) view.findViewById(R.id.btn_Save);
        _cancelBtn = (Button) view.findViewById(R.id.btn_Cancel);

        _breakfast.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DisableAllRadioButtons();
                _breakfast.setChecked(true);
            }
        });

        _lunch.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DisableAllRadioButtons();
                _lunch.setChecked(true);
            }
        });

        _dinner.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DisableAllRadioButtons();
                _dinner.setChecked(true);
            }
        });

        _desert.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DisableAllRadioButtons();
                _desert.setChecked(true);
            }
        });

        _appitizer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DisableAllRadioButtons();
                _appitizer.setChecked(true);
            }
        });

        _other.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DisableAllRadioButtons();
                _other.setChecked(true);
            }
        });

        _saveBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(_breakfast.isChecked()) {
                    Toast.makeText(getContext(), "choice: Breakfast",
                            Toast.LENGTH_SHORT).show();
                } else if(_lunch.isChecked()) {
                    Toast.makeText(getContext(), "choice: Lunch",
                            Toast.LENGTH_SHORT).show();
                } else if(_dinner.isChecked()) {
                    Toast.makeText(getContext(), "choice: Dinner",
                            Toast.LENGTH_SHORT).show();
                }else if(_desert.isChecked()) {
                    Toast.makeText(getContext(), "choice: Desert",
                            Toast.LENGTH_SHORT).show();
                }else if(_appitizer.isChecked()) {
                    Toast.makeText(getContext(), "choice: Appetizer",
                            Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getContext(), "choice: Other",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

        _cancelBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                getDialog().dismiss();
            }
        });

        return view;
    }

    private void DisableAllRadioButtons(){
        _breakfast.setChecked(false);
        _lunch.setChecked(false);
        _dinner.setChecked(false);
        _desert.setChecked(false);
        _appitizer.setChecked(false);
        _other.setChecked(false);
    }

}
