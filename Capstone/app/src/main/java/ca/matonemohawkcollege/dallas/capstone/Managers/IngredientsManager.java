package ca.matonemohawkcollege.dallas.capstone.Managers;

import android.app.Activity;
import android.os.AsyncTask;

import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.OkHttpClientFactory;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.squareup.okhttp.OkHttpClient;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import ca.matonemohawkcollege.dallas.capstone.Base.UserBase;
import ca.matonemohawkcollege.dallas.capstone.Models.Ingredients;
import ca.matonemohawkcollege.dallas.capstone.Models.Users;
import ca.matonemohawkcollege.dallas.capstone.R;

/**
 * Created by Dallas on 1/5/2018.
 */

public class IngredientsManager extends Activity {

    private Activity myActivity;

    /**
     * Mobile Service Client reference
     */
    private MobileServiceClient mClient;

    /**
     * Mobile Service Table used to access data
     */
    private MobileServiceTable<Ingredients> mIngredientsTable;

    public IngredientsManager(Activity inActivity) {
        myActivity = inActivity;

        try {
            // Create the Mobile Service Client instance, using the provided

            // Mobile Service URL and key
            mClient = new MobileServiceClient(
                    "https://simmer.azurewebsites.net/",
                    myActivity);

            // Extend timeout from default of 10s to 20s
            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
                @Override
                public OkHttpClient createOkHttpClient() {
                    OkHttpClient client = new OkHttpClient();
                    client.setReadTimeout(20, TimeUnit.SECONDS);
                    client.setWriteTimeout(20, TimeUnit.SECONDS);
                    return client;
                }
            });

            // Get the Mobile Service Table instance to use

            mIngredientsTable = mClient.getTable("Ingredients",Ingredients.class);

            // Load the items from the Mobile Service
            refreshItemsFromTable();

        } catch (MalformedURLException e) {

        } catch (Exception e) {

        }

    }

    public void SetUserNameByUserID(String userID) {

        UserBase userBase = new UserBase(this);

        String uri =  myActivity.getString(R.string.url);
        // Filter - numbers (Year) no quotes on value, strings have quotes on value
        uri += String.format("Users?$filter=User_ID eq '%s'",userID);
        userBase.execute(uri);

    }

    private void refreshItemsFromTable() {

        // Get the items that weren't marked as completed and add them in the
        // adapter

        new AsyncTask<String, Void, String>(){
            @Override
            protected String doInBackground(String... params) {
                String test = "";
                try {
                    final List<Ingredients> results = refreshItemsFromMobileServiceTable();

                    //Offline Sync
                    //final List<ToDoItem> results = refreshItemsFromMobileServiceTableSyncTable();

                    for (Ingredients ingredient : results) {
                        System.out.println(ingredient.getIngredientsName());
                        System.out.println(ingredient.getRecipeID());
                    }

                } catch (final Exception e){
                    System.out.println(e);
                }

                return test;
            }
            @Override
            protected void onPostExecute(String result) {
                // remember this method gets called on main thread
                //letsCallFogsMethod(makeValue); //call your method and pass the make value here :)

                String x = result;
            }
        }.execute();
    }

    private List<Ingredients> refreshItemsFromMobileServiceTable() throws ExecutionException, InterruptedException {
        return mIngredientsTable.where().field("id").
                eq("93b9e69b-7713-4a0b-8863-ab84d810481e").execute().get();
    }
}
