package ca.matonemohawkcollege.dallas.capstone.Dialogs;


import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import ca.matonemohawkcollege.dallas.capstone.R;

public class RecipeTagsDialog extends DialogFragment {

    private CheckBox _veganCheck;
    private CheckBox _vegetarianCheck;
    private CheckBox _halalCheck;
    private CheckBox _kosherCheck;
    private CheckBox _dairyFreeCheck;
    private CheckBox _lowFatCheck;
    private Button _saveBtn;
    private Button _cancelBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_recipe_tags_dialog, null);

        _veganCheck = (CheckBox) view.findViewById(R.id.chk_Vegan);
        _vegetarianCheck = (CheckBox) view.findViewById(R.id.chk_Vegetarian);
        _halalCheck = (CheckBox) view.findViewById(R.id.chk_Halal);
        _kosherCheck = (CheckBox) view.findViewById(R.id.chk_Kosher);
        _dairyFreeCheck = (CheckBox) view.findViewById(R.id.chk_DairyFree);
        _lowFatCheck = (CheckBox) view.findViewById(R.id.chk_LowFat);
        _saveBtn = (Button) view.findViewById(R.id.btn_Save);
        _cancelBtn = (Button) view.findViewById(R.id.btn_Cancel);

        _saveBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Toast.makeText(getContext(), "Tags", Toast.LENGTH_SHORT).show();
            }
        });

        _cancelBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                getDialog().dismiss();
            }
        });

        return view;
    }
}
