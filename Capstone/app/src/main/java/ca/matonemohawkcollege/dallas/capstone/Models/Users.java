package ca.matonemohawkcollege.dallas.capstone.Models;

/**
 * Created by Dallas on 1/5/2018.
 */

public class Users {

    @com.google.gson.annotations.SerializedName("id")
    private String id;

    @com.google.gson.annotations.SerializedName("user_ID")
    private String user_ID;

    @com.google.gson.annotations.SerializedName("user_Name")
    private String user_Name;

    @com.google.gson.annotations.SerializedName("user_Description")
    private String user_Description;

    @com.google.gson.annotations.SerializedName("user_ProfilePicture")
    private byte[] user_ProfilePicture;

    public Users() {

    }

    public String getId() {
        return id;
    }

    public final void setId(String text) {
        id = text;
    }

    public String getUserID() {
        return user_ID;
    }

    public final void setUserID(String text) {
        user_ID = text;
    }


    public String getUserName() {
        return user_Name;
    }

    public final void setUserName(String text) { user_Name = text; }


    public String getUser_Description() {
        return user_Description;
    }

    public void setUser_Description(String text) {
        user_Description = text;
    }


    public byte[] getUser_ProfilePicture() {
        return user_ProfilePicture;
    }

    public void setUser_ProfilePicture(byte[] picture) { user_ProfilePicture = picture; }
}
