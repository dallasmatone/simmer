package ca.matonemohawkcollege.dallas.capstone.Models;

import java.util.UUID;

/**
 * Created by Dallas on 1/5/2018.
 */

public class Steps {

    @com.google.gson.annotations.SerializedName("id")
    public String id;

    @com.google.gson.annotations.SerializedName("recipe_ID")
    public String recipe_ID;

    @com.google.gson.annotations.SerializedName("step_Position")
    public int step_Position;

    @com.google.gson.annotations.SerializedName("step_Text")
    public String step_Text;

    public Steps() {

    }

    public String getId() {
        return id;
    }

    public final void setId(String text) { id = text; }


    public String getRecipeID() {
        return recipe_ID;
    }

    public final void setRecipeID(String text) {
        recipe_ID = text;
    }


    public int getStepPositon() {
        return step_Position;
    }

    public final void setStepPositione(int text) { step_Position = text; }


    public String getStepText() {
        return step_Text;
    }

    public final void setStepText(String text) {
        step_Text = text;
    }
}
