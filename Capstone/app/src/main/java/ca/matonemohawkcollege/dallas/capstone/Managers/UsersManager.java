package ca.matonemohawkcollege.dallas.capstone.Managers;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.EditText;
import android.widget.ListView;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.gson.Gson;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.OkHttpClientFactory;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.sync.MobileServiceSyncContext;
import com.microsoft.windowsazure.mobileservices.table.sync.localstore.ColumnDataType;
import com.microsoft.windowsazure.mobileservices.table.sync.localstore.MobileServiceLocalStoreException;
import com.microsoft.windowsazure.mobileservices.table.sync.localstore.SQLiteLocalStore;
import com.microsoft.windowsazure.mobileservices.table.sync.synchandler.SimpleSyncHandler;
import com.squareup.okhttp.OkHttpClient;
import com.twitter.sdk.android.core.models.User;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import ca.matonemohawkcollege.dallas.capstone.Base.UserBase;
import ca.matonemohawkcollege.dallas.capstone.Models.Users;
import ca.matonemohawkcollege.dallas.capstone.R;
import ca.matonemohawkcollege.dallas.capstone.Views.LoginScreen;

import static com.microsoft.windowsazure.mobileservices.table.query.QueryOperations.val;

/**
 * Created by Dallas on 1/5/2018.
 */

public class UsersManager  extends Activity {

    private Activity myActivity;
    public List<Users> UsersList;
    private String _userID;
    private String _userName;
    private List<Users> _usersList;
    private GoogleSignInAccount _account;
    boolean isQueryDone = false;

    /**
     * Mobile Service Client reference
     */
    private MobileServiceClient mClient;

    /**
     * Mobile Service Table used to access data
     */
    private MobileServiceTable<Users> mUsersTable;

    public UsersManager(Activity inActivity) {
        myActivity = inActivity;

        try {
            // Create the Mobile Service Client instance, using the provided
            UsersList = new ArrayList<Users>();
            // Mobile Service URL and key
            mClient = new MobileServiceClient(
                    "https://simmer.azurewebsites.net/",
                    myActivity);

            // Extend timeout from default of 10s to 20s
            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
                @Override
                public OkHttpClient createOkHttpClient() {
                    OkHttpClient client = new OkHttpClient();
                    client.setReadTimeout(20, TimeUnit.SECONDS);
                    client.setWriteTimeout(20, TimeUnit.SECONDS);
                    return client;
                }
            });

            // Get the Mobile Service Table instance to use

            mUsersTable = mClient.getTable("Users",Users.class);


        } catch (MalformedURLException e) {

        } catch (Exception e) {

        }

    }

    public void GetUserNameByUserID(GoogleSignInAccount account) {
        try {
            // Load the items from the Mobile Service
            _account = account;
            _userID = account.getEmail();
            _userName = account.getDisplayName();
            LoginInformation();
        } catch (Exception e){

        }
    }

    private void LoginInformation() {
        new AsyncTask<String, Void, String>(){
            @Override
            protected String doInBackground(String... params) {
                String test = "";
                try {
                    _usersList = refreshItemsFromMobileServiceTable();

                    for (Users user : _usersList) {
                        UsersList.add(user);
                    }

                } catch (final Exception e){
                    System.out.println(e);
                }

                return test;
            }
            @Override
            protected void onPostExecute(String result) {
                if(UsersList.size() != 0)
                    ((LoginScreen) myActivity).accessDashboard(_account);
                else{
                    CreateUser();
                }
            }
        }.execute();
    }

    private void CreateUser() {
        if (mClient == null) {
            return;
        }

        // Create a new item
        final Users item = new Users();

        item.setId(UUID.randomUUID().toString());
        item.setUserID(_userID);
        item.setUserName(_userName);

        // Insert the new item
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final Users entity = addItemInTable(item);
                } catch (final Exception e) {
                    System.out.println(e);
                }
                return null;
            }
        };

        runAsyncTask(task);
        ((LoginScreen) myActivity).accessDashboard(_account);
    }

    public Users addItemInTable(Users item) throws ExecutionException, InterruptedException {
        Users entity = mUsersTable.insert(item).get();
        return entity;
    }

    private AsyncTask<Void, Void, Void> runAsyncTask(AsyncTask<Void, Void, Void> task) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            return task.execute();
        }
    }

    private List<Users> refreshItemsFromMobileServiceTable() throws ExecutionException, InterruptedException {
        return mUsersTable.where().field("user_ID").
                eq(_userID).execute().get();
    }

}
