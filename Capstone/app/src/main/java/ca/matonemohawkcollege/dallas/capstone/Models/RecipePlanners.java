package ca.matonemohawkcollege.dallas.capstone.Models;

import java.sql.Date;
import java.util.UUID;

/**
 * Created by Dallas on 1/5/2018.
 */

public class RecipePlanners {

    @com.google.gson.annotations.SerializedName("id")
    public String id;

    @com.google.gson.annotations.SerializedName("recipePlanner_ID")
    public String recipePlanner_ID;

    @com.google.gson.annotations.SerializedName("recipe_ID")
    public String recipe_ID;

    @com.google.gson.annotations.SerializedName("recipePlanner_RecipeName")
    public String recipePlanner_RecipeName;

    @com.google.gson.annotations.SerializedName("recipePlanner_Date")
    public String recipePlanner_Date;



    public RecipePlanners() {

    }

    public String getId() {
        return id;
    }

    public final void setId(String text) { id = text; }


    public String getRecipePlannerID() {
        return recipePlanner_ID;
    }

    public final void setRecipePlannerID(String text) { recipePlanner_ID = text; }


    public String getRecipeID() {
        return recipe_ID;
    }

    public final void setRecipeID(String text) {
        recipe_ID = text;
    }


    public String getRecipePlannerRecipeName() {
        return recipePlanner_RecipeName;
    }

    public final void setRecipePlannerRecipeName(String text) { recipePlanner_RecipeName = text; }


    public String getRecipePlannerDate() {
        return recipePlanner_Date;
    }

    public final void setRecipePlannerDate(String text) { recipePlanner_Date = text; }
}
