package ca.matonemohawkcollege.dallas.capstone;

/**
 * Created by Dallas on 1/1/2018.
 */

public class ColumnConstants {
    public static final String RECIPE_NAME_COLUMN = "First";
    public static final String RECIPE_DESCRIPTION_COLUMN = "Second";
    public static final String RECIPE_RATING_COLUMN = "Third";
    public static final String RECIPE_ID = "Fourth";
    public static final String RECIPE_IS_FAVOURITE = "Fifth";
    public static final String USER_ID = "Sixth";
    public static final String ID = "Seventh";
}
