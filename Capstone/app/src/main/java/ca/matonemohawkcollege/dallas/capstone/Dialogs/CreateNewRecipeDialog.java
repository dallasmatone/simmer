package ca.matonemohawkcollege.dallas.capstone.Dialogs;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import ca.matonemohawkcollege.dallas.capstone.Managers.RecipesManager;
import ca.matonemohawkcollege.dallas.capstone.R;
import ca.matonemohawkcollege.dallas.capstone.Views.RecipeScreen;


/**
 * A simple {@link Fragment} subclass.
 */
public class CreateNewRecipeDialog extends DialogFragment {

    private EditText _recipeName;
    private Button _createBtn;
    private Button _cancelBtn;
    private RecipesManager _recipesManager;
    private String _userID;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_create_new_recipe_dialog, null);

        _recipeName = (EditText) view.findViewById(R.id.txt_recipeName);
        _createBtn = (Button) view.findViewById(R.id.btn_Create);
        _cancelBtn = (Button) view.findViewById(R.id.btn_Cancel);
        _recipesManager = new RecipesManager(getActivity());
        _userID = getArguments().getString("UserID");

        _createBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (_recipeName.getText().toString().trim().equalsIgnoreCase("")) {
                    _recipeName.setError("This field can not be blank!");
                }else {
                    _recipesManager.CreateNewRecipe(_userID, _recipeName.getText().toString());
                    dismiss();
                }
            }
        });

        _cancelBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                getDialog().dismiss();
            }
        });

        return view;
    }

}
