package ca.matonemohawkcollege.dallas.capstone.Tabs;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.ToggleButton;

import java.math.BigDecimal;

import ca.matonemohawkcollege.dallas.capstone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class DistanceTab extends Fragment {

    public Spinner DistanceSpinnerOne;
    public Spinner DistanceSpinnerTwo;
    public ToggleButton DistanceToggleOne;
    public ToggleButton DistanceToggleTwo;
    public ImageButton SwapMeasurementsBtn;
    public EditText DistanceOneText;
    public EditText DistanceTwoText;

    private static final double MILLIMETER = .001;
    private static final double CENTIMETER = .01;
    private static final double DECIMETER = .1;
    private static final double METER = 1;

    private static final double INCH = 0.0254;
    private static final double FOOT = 0.3048;
    private static final double YARD = 0.9144;

    public DistanceTab() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_distance_tab, container, false);
        setHasOptionsMenu(false);

        //Initalize Public Variables
        DistanceSpinnerOne = (Spinner) view.findViewById(R.id.spnDistanceOne);
        DistanceSpinnerTwo = (Spinner) view.findViewById(R.id.spnDistanceTwo);
        DistanceToggleOne = (ToggleButton) view.findViewById(R.id.tglDistanceOne);
        DistanceToggleTwo = (ToggleButton) view.findViewById(R.id.tglDistanceTwo);
        SwapMeasurementsBtn = (ImageButton) view.findViewById(R.id.btnSwapMeasurments);
        DistanceOneText = (EditText) view.findViewById(R.id.txtDistanceOne);
        DistanceTwoText = (EditText) view.findViewById(R.id.txtDistanceTwo);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.array_MetricDistanceMeasurments));
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        DistanceSpinnerOne.setAdapter(adapter);
        DistanceSpinnerTwo.setAdapter(adapter);

        DistanceToggleOne.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ToggleMetricImperial(isChecked, getResources().getStringArray(R.array.array_ImperialDistanceMeasurments), true);
                } else {
                    ToggleMetricImperial(isChecked, getResources().getStringArray(R.array.array_MetricDistanceMeasurments), true);
                }
            }
        });

        DistanceToggleTwo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ToggleMetricImperial(isChecked, getResources().getStringArray(R.array.array_ImperialDistanceMeasurments), false);
                } else {
                    ToggleMetricImperial(isChecked, getResources().getStringArray(R.array.array_MetricDistanceMeasurments), false);
                }
            }
        });

        DistanceSpinnerOne.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                ConvertMeasurment(DistanceOneText.getText().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        DistanceSpinnerTwo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                ConvertMeasurment(DistanceOneText.getText().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        SwapMeasurementsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int measurementOne = DistanceSpinnerOne.getSelectedItemPosition();
                int measurementTwo = DistanceSpinnerTwo.getSelectedItemPosition();
                boolean toggleOne = DistanceToggleOne.isChecked();
                boolean toggleTwo = DistanceToggleTwo.isChecked();

                DistanceToggleOne.setChecked(toggleTwo);
                DistanceToggleTwo.setChecked(toggleOne);
                DistanceSpinnerOne.setSelection(measurementTwo);
                DistanceSpinnerTwo.setSelection(measurementOne);

                ConvertMeasurment(DistanceOneText.getText().toString());
            }
        });


        DistanceOneText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                ConvertMeasurment(s.toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        return view;
    }

    private void ToggleMetricImperial(boolean isChecked, String [] resourceStringArray, boolean isControlOne){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, resourceStringArray);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        if(isControlOne)
            DistanceSpinnerOne.setAdapter(adapter);
        else
            DistanceSpinnerTwo.setAdapter(adapter);
    }

    private void ConvertMeasurment(String measurmentToConvert){
        if(!measurmentToConvert.isEmpty()) {
            if(measurmentToConvert.equals(".")) {
                measurmentToConvert = "0.";
            }
            double valueToConvert = Double.valueOf(measurmentToConvert);
            double meters = ConvertDistance(valueToConvert);
            DistanceTwoText.setText(String.valueOf(ConvertFromMeters(meters)));
        }
        else{
            DistanceTwoText.setText("0");
        }
    }

    private double ConvertDistance(double distanceOne){
        String distanceOneType = DistanceSpinnerOne.getSelectedItem().toString();

        double meters = 0;

        switch(distanceOneType){
            case "mm":
                meters = distanceOne * MILLIMETER;
                break;
            case "cm":
                meters = distanceOne * CENTIMETER;
                break;
            case "dm":
                meters = distanceOne * DECIMETER;
                break;
            case "m":
                meters = distanceOne * METER;
                break;
            case "in":
                meters = distanceOne * INCH;
                break;
            case "ft":
                meters = distanceOne * FOOT;
                break;
            case "yd":
                meters = distanceOne * YARD;
                break;
            default:
                meters = 0;
                break;
        }

        return meters;
    }

    private double ConvertFromMeters(double meters) {
        String distanceTwoType = DistanceSpinnerTwo.getSelectedItem().toString();
        double converted = 0;

        switch (distanceTwoType) {
            case "mm":
                converted = meters / MILLIMETER;
                break;
            case "cm":
                converted = meters / CENTIMETER;
                break;
            case "dm":
                converted = meters / DECIMETER;
                break;
            case "m":
                converted = meters / METER;
                break;
            case "in":
                converted = meters / INCH;
                break;
            case "ft":
                converted = meters / FOOT;
                break;
            case "yd":
                converted = meters / YARD;
                break;
            default:
                converted = 0;
                break;
        }

        BigDecimal bd = new BigDecimal(converted);
        bd = bd.setScale(4, BigDecimal.ROUND_HALF_UP);
        converted = bd.doubleValue();
        return converted;
    }

}
