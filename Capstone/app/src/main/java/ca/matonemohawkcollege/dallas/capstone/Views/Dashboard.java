package ca.matonemohawkcollege.dallas.capstone.Views;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import at.markushi.ui.CircleButton;
import ca.matonemohawkcollege.dallas.capstone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Dashboard extends Fragment {

    private TextView _recipeName;
    private TextView _recipeDescription;

    public Dashboard() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        getActivity().setTitle("Dashboard");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        _recipeName = (TextView) view.findViewById(R.id.lbl_recipeName);
        _recipeDescription = (TextView) view.findViewById(R.id.lbl_recipeDescription);

        CircleButton recipeOfTheDayBtn = (CircleButton) view.findViewById(R.id.circlebtn_dahsboardRecipe);
        recipeOfTheDayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new RecipeScreen();
                Bundle bundle = new Bundle();
                bundle.putString("recipeName", _recipeName.getText().toString());
                bundle.putString("CRUD", "View");
                fragment.setArguments(bundle);
                FragmentManager fm = getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.content_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return view;
    }
}
