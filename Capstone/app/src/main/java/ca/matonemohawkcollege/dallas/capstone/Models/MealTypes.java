package ca.matonemohawkcollege.dallas.capstone.Models;

import java.util.UUID;

/**
 * Created by Dallas on 1/5/2018.
 */

public class MealTypes {

    @com.google.gson.annotations.SerializedName("id")
    public String id;

    @com.google.gson.annotations.SerializedName("recipe_ID")
    public String recipe_ID;

    @com.google.gson.annotations.SerializedName("mealType_IsBreakfast")
    public boolean mealType_IsBreakfast;

    @com.google.gson.annotations.SerializedName("mealType_IsLunch")
    public boolean mealType_IsLunch;

    @com.google.gson.annotations.SerializedName("mealType_IsDinner")
    public boolean mealType_IsDinner;

    @com.google.gson.annotations.SerializedName("mealType_IsDesert")
    public boolean mealType_IsDesert;

    @com.google.gson.annotations.SerializedName("mealType_IsAppetizers")
    public boolean mealType_IsAppetizers;

    @com.google.gson.annotations.SerializedName("mealType_IsOther")
    public boolean mealType_IsOther;

    public MealTypes() {

    }

    public String getId() {
        return id;
    }

    public final void setId(String text) { id = text; }


    public String getRecipeID() {
        return recipe_ID;
    }

    public final void setRecipeID(String text) {
        recipe_ID = text;
    }

    public boolean getTagIsBreakfast(){
        return mealType_IsBreakfast;
    }

    public final void setTagIsBreakfast(boolean value){
        mealType_IsBreakfast = value;
    }

    public boolean getTagIsLunch(){
        return mealType_IsLunch;
    }

    public final void setTagIsLunch(boolean value){
        mealType_IsLunch = value;
    }

    public boolean getTagIsDinner(){
        return mealType_IsDinner;
    }

    public final void setTagIsDinner(boolean value){
        mealType_IsDinner = value;
    }

    public boolean getTagIsDesert(){
        return mealType_IsDesert;
    }

    public final void setTagIsDesert(boolean value){
        mealType_IsDesert = value;
    }

    public boolean getTagIsAppitizers(){
        return mealType_IsAppetizers;
    }

    public final void setTagIsAppitizers(boolean value){
        mealType_IsAppetizers = value;
    }

    public boolean getTagIsOther(){ return mealType_IsOther; }

    public final void setTagIsOther(boolean value){
        mealType_IsOther = value;
    }
}
