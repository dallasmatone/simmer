package ca.matonemohawkcollege.dallas.capstone.Views;

import android.content.Intent;
import android.os.UserManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.models.User;

import java.util.ArrayList;
import java.util.List;

import at.markushi.ui.CircleButton;
import ca.matonemohawkcollege.dallas.capstone.Dialogs.CreateNewRecipeDialog;
import ca.matonemohawkcollege.dallas.capstone.Managers.RecipesManager;
import ca.matonemohawkcollege.dallas.capstone.Managers.UsersManager;
import ca.matonemohawkcollege.dallas.capstone.Models.Recipes;
import ca.matonemohawkcollege.dallas.capstone.R;

public class NavBar extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener {

    private CircleButton UserProfileButton;
    private TextView _userNameText;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private GoogleApiClient googleApiClient;
    private RecipesManager _recipesManager;
    private static final int REQ_CODE = 9001;
    private String _userID;

    public List<Recipes> RecipeList;
    //Entities
    public Recipes _recipes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navbar);
        RecipeList = new ArrayList<>();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        _recipesManager = new RecipesManager(this);
        _recipes = new Recipes();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_container, new Dashboard());
        fragmentTransaction.commit();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        Intent intent = getIntent();
        String userName = intent.getStringExtra("UserName");
        _userID = intent.getStringExtra("UserID");
        _userNameText = (TextView) navigationView.getHeaderView(0).findViewById(R.id.usernamelbl);
        if(userName != null)
            _userNameText.setText(userName);

        UserProfileButton = (CircleButton) navigationView.getHeaderView(0).findViewById(R.id.userProfilePicBtn);
        UserProfileButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Bundle bundle = new Bundle();
                bundle.putString("UserName", _userNameText.getText().toString());
                Fragment fragment = new UserProfileScreen();
                fragment.setArguments(bundle);
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.content_container, fragment);
                fragmentTransaction.commit();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage
                (this,this).addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

         @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_logout) {
                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        getApplication().startActivity(new Intent(getApplication(), LoginScreen.class));
                        finish();
                    }
                });
            }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;

        if (id == R.id.nav_Dashboard) {
            fragment = new Dashboard();
        } else if (id == R.id.nav_Recipes) {
            _recipesManager.GetUsersRecipes(_userID);
        } else if (id == R.id.nav_Planner) {
            fragment = new RecipePlannerScreen();
        } else if (id == R.id.nav_Calculator) {
            fragment = new ConversionCalculatorScreen();
        } else if (id == R.id.nav_OnlineRecipes) {
            fragment = new SimmerOnlineScreen();
        } else if (id == R.id.nav_CreateRecipe) {
            Bundle bundle = new Bundle();
            bundle.putString("UserID", _userID);
            CreateNewRecipeDialog createNewRecipeDialog = new CreateNewRecipeDialog();
            createNewRecipeDialog.setArguments(bundle);
            createNewRecipeDialog.show(fragmentManager, "CreateNewRecipeDialog");
        } else if (id == R.id.nav_Favourites) {
            fragment = new FavouriteRecipesScreen();
        }

        if(fragment != null){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_container, fragment);
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    //https://stackoverflow.com/questions/36889141/hide-keyboard-in-fragment-on-outside-click
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void createNewRecipeScreen(Recipes recipe){
        Fragment fragment = new RecipeScreen();
        Bundle bundle = new Bundle();
        bundle.putString("recipeName", recipe.getRecipeName());
        bundle.putString("UserID", recipe.getUserID());
        bundle.putString("RecipeID", recipe.getRecipeID());
        bundle.putString("ID", recipe.getId());
        bundle.putString("CRUD", "Add");
        fragment.setArguments(bundle);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.content_container, fragment);
        fragmentTransaction.commit();
    }

    public void viewRecipes(List<Recipes> recipe) {
        RecipeList = recipe;
        Fragment fragment = new RecipeSearchScreen();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.content_container, fragment);
        fragmentTransaction.commit();
    }
}
