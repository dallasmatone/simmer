package ca.matonemohawkcollege.dallas.capstone.Views;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ca.matonemohawkcollege.dallas.capstone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class SimmerOnlineScreen extends Fragment {


    public SimmerOnlineScreen() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        getActivity().setTitle("Simmer Online");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_simmer_online_screen, container, false);
    }

}
