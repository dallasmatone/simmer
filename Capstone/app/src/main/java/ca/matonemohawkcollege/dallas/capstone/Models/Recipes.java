package ca.matonemohawkcollege.dallas.capstone.Models;

import java.util.UUID;

/**
 * Created by Dallas on 1/5/2018.
 */

public class Recipes {

    @com.google.gson.annotations.SerializedName("id")
    public String id;

    @com.google.gson.annotations.SerializedName("recipe_ID")
    public String recipe_ID;

    @com.google.gson.annotations.SerializedName("user_ID")
    public String user_ID;

    @com.google.gson.annotations.SerializedName("recipe_Name")
    public String recipe_Name;

    @com.google.gson.annotations.SerializedName("recipe_Description")
    public String recipe_Description;

    @com.google.gson.annotations.SerializedName("recipe_Notes")
    public String recipe_Notes;

    @com.google.gson.annotations.SerializedName("recipe_Rating")
    public float recipe_Rating;

    @com.google.gson.annotations.SerializedName("recipe_IsDeleted")
    public boolean recipe_IsDeleted;

    @com.google.gson.annotations.SerializedName("recipe_IsFavourite")
    public boolean recipe_IsFavourite;

    @com.google.gson.annotations.SerializedName("recipe_Creator")
    public String recipe_Creator;

    @com.google.gson.annotations.SerializedName("recipe_Picture")
    public byte[] recipe_Picture;


    public Recipes() {

    }

    public String getId() {
        return id;
    }

    public final void setId(String text) { id = text; }


    public String getRecipeID() {
        return recipe_ID;
    }

    public final void setRecipeID(String text) {
        recipe_ID = text;
    }


    public String getUserID() {
        return user_ID;
    }

    public final void setUserID(String text) { user_ID = text; }


    public String getRecipeName() { return recipe_Name; }

    public final void setRecipeName(String text) { recipe_Name = text; }


    public String getRecipeDescription() { return recipe_Description; }

    public final void setRecipeDescription(String text) { recipe_Description = text; }


    public String getRecipeNotes() { return recipe_Notes; }

    public final void setRecipeNotes(String text) { recipe_Notes = text; }


    public float getRecipeRating() { return recipe_Rating; }

    public final void setRecipeRating(float rating) { recipe_Rating = rating; }


    public boolean getIsDeleted() { return recipe_IsDeleted; }

    public final void setIsDeleted(boolean value) { recipe_IsDeleted = value; }


    public boolean getIsFavorutie() { return recipe_IsFavourite; }

    public final void setIsFavorutie(boolean value) { recipe_IsFavourite = value; }


    public String getRecipeCreator() { return recipe_Creator; }

    public final void setRecipeCreator(String text) { recipe_Creator = text; }


    public byte[] getRecipePicture() { return recipe_Picture; }

    public final void setRecipePicture(byte[] pic) { recipe_Picture = pic; }


}
