package ca.matonemohawkcollege.dallas.capstone.Tabs;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.math.BigDecimal;

import ca.matonemohawkcollege.dallas.capstone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class TempuratureTab extends Fragment {

    EditText TempuratureOneText;
    EditText TempuratureTwoText;
    ImageButton SwapTempuraturesBtn;
    TextView TempOneLabel;
    TextView TempTwoLabel;

    public TempuratureTab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tempurature_tab, container, false);
        setHasOptionsMenu(false);

        TempuratureOneText = (EditText) view.findViewById(R.id.txtTempOne);
        TempuratureTwoText = (EditText) view.findViewById(R.id.txtTempTwo);
        SwapTempuraturesBtn = (ImageButton) view.findViewById(R.id.btnSwapTempuratures);
        TempOneLabel = (TextView) view.findViewById(R.id.lblTempOne);
        TempTwoLabel = (TextView) view.findViewById(R.id.lblTempTwo);

        SwapTempuraturesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempLabel = TempTwoLabel.getText().toString();
                TempTwoLabel.setText(TempOneLabel.getText().toString());
                TempOneLabel.setText(tempLabel);
                ConvertTempurature();
            }
        });


        TempuratureOneText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                ConvertTempurature();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        return view;
    }

    private void ConvertTempurature() {
        String temp = TempuratureOneText.getText().toString();
        double convertedTemp;

        if(!temp.equals("")) {
            double tempToConvert = Double.valueOf(temp);
            if (TempOneLabel.getText().toString().equals("C°"))
                convertedTemp = ConvertCelsiusToFahrenheit(tempToConvert);
            else
                convertedTemp = ConvertFahrenheitToCelsius(tempToConvert);

            BigDecimal bd = new BigDecimal(convertedTemp);
            bd = bd.setScale(1, BigDecimal.ROUND_HALF_UP);
            convertedTemp = bd.doubleValue();
        }
        else{
            convertedTemp = 0;
        }
        TempuratureTwoText.setText(String.valueOf(convertedTemp));
    }

    private double ConvertFahrenheitToCelsius(double tempToConvert) {
        double convertedTemp = (((tempToConvert - 32) * 5) / 9);
        return convertedTemp;
    }

    private double ConvertCelsiusToFahrenheit(double tempToConvert) {
        double convertedTemp = (((tempToConvert * 9) / 5) + 32);
        return convertedTemp;
    }

}
