package ca.matonemohawkcollege.dallas.capstone.Views;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.HashMap;

import ca.matonemohawkcollege.dallas.capstone.R;
import ca.matonemohawkcollege.dallas.capstone.RecipeListViewAdapter;

import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_DESCRIPTION_COLUMN;
import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_ID;
import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_NAME_COLUMN;
import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_RATING_COLUMN;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavouriteRecipesScreen extends Fragment {

    private ArrayList<HashMap<String, String>> _recipeList;
    private ListView _recipeListView;
    private ImageButton _filterRecipes;
    private RecipeListViewAdapter _adapter;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.recipe_search, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                ArrayList<HashMap<String, String>> tempList = new ArrayList<>();

                for (HashMap<String, String> temp : _recipeList){
                    if(temp.get(RECIPE_NAME_COLUMN).toLowerCase().contains(s.toLowerCase())){
                        tempList.add(temp);
                    }
                }

               _adapter = new RecipeListViewAdapter(getActivity(), tempList);
                _recipeListView.setAdapter(_adapter);

                return true;
            }
        });

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Favourite Recipes");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favourite_recipes_screen, container, false);
        setHasOptionsMenu(true);

        _recipeListView = (ListView)view.findViewById(R.id.lv_RecipeList);
        _filterRecipes = (ImageButton) view.findViewById(R.id.btn_FilterTags);

        _recipeList = new ArrayList<HashMap<String,String>>();

        HashMap<String,String> temp=new HashMap<String, String>();
        temp.put(RECIPE_NAME_COLUMN, "Turkey with a side of shrimp");
        temp.put(RECIPE_DESCRIPTION_COLUMN, "This delicacy is a delcicious materpiece. Couldnt have made a better dish if i tried!");
        temp.put(RECIPE_RATING_COLUMN, "3.5");
        temp.put(RECIPE_ID, "0");
        _recipeList.add(temp);

        HashMap<String,String> temp2=new HashMap<String, String>();
        temp2.put(RECIPE_NAME_COLUMN, "Corn On The Cob");
        temp2.put(RECIPE_DESCRIPTION_COLUMN, "Nicely cooked corn, Rosted to perfection!");
        temp2.put(RECIPE_RATING_COLUMN, "4");
        temp2.put(RECIPE_ID, "1");
        _recipeList.add(temp2);

        HashMap<String,String> temp3=new HashMap<String, String>();
        temp3.put(RECIPE_NAME_COLUMN, "Scrambled Eggs");
        temp3.put(RECIPE_DESCRIPTION_COLUMN, "");
        temp3.put(RECIPE_RATING_COLUMN, "");
        temp3.put(RECIPE_ID, "2");
        _recipeList.add(temp3);


        _adapter = new RecipeListViewAdapter(getActivity(), _recipeList);
        _recipeListView.setAdapter(_adapter);

        _recipeListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id)
            {
                Animation animation1 = new AlphaAnimation(0.3f, 1.0f);
                animation1.setDuration(1000);
                view.startAnimation(animation1);

                int counter = 0;
                String recipeName = "";
                String recipeRating = "";
                String recipeDescription = "";

                HashMap<String,String> item = (HashMap<String,String>) _adapter.getItem(position);

                Fragment fragment = new RecipeScreen();
                Bundle bundle = new Bundle();
                bundle.putString("recipeName", item.get(RECIPE_NAME_COLUMN));
                bundle.putString("recipeDescription", item.get(RECIPE_DESCRIPTION_COLUMN));
                bundle.putString("recipeRating", item.get(RECIPE_RATING_COLUMN));
                bundle.putBoolean("recipeFavourite", true);
                bundle.putString("CRUD", "View");
                fragment.setArguments(bundle);

                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.content_container, fragment);
                fragmentTransaction.addToBackStack("RecipeSearch");
                fragmentTransaction.commit();
                setHasOptionsMenu(false);
            }

        });

        _filterRecipes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
            }
        });

        return view;
    }

}
