package ca.matonemohawkcollege.dallas.capstone.Tabs;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.ToggleButton;

import java.math.BigDecimal;

import ca.matonemohawkcollege.dallas.capstone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class VolumnTab extends Fragment {

    public Spinner VolumnSpinnerOne;
    public Spinner VolumnSpinnerTwo;
    public ImageButton SwapVolumnsBtn;
    public EditText VolumnOneText;
    public EditText VolumnTwoText;

    private static final double MILLILITERS = 1;
    private static final double TABLESPOONS = 0.0676;
    private static final double TEASPOONS = 0.2029;
    private static final double OUNCES = 0.0352;
    private static final double CUPS = 0.0042;
    private static final double PINTS = 0.0018;
    private static final double QUARTS = 0.00022;
    private static final double GALLONS = 0.000227;
    private static final double LITERS = 0.001;

    public VolumnTab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_volumn_tab, container, false);

        //Initalize Public Variables
        VolumnSpinnerOne = (Spinner) view.findViewById(R.id.spnVolumnOne);
        VolumnSpinnerTwo = (Spinner) view.findViewById(R.id.spnVolumnTwo);
        SwapVolumnsBtn = (ImageButton) view.findViewById(R.id.btnSwapVolumns);
        VolumnOneText = (EditText) view.findViewById(R.id.txtVolumnOne);
        VolumnTwoText = (EditText) view.findViewById(R.id.txtVolumnTwo);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.array_VolumnMeasurments));
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        VolumnSpinnerOne.setAdapter(adapter);
        VolumnSpinnerTwo.setAdapter(adapter);

        SwapVolumnsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int measurementOne = VolumnSpinnerOne.getSelectedItemPosition();
                int measurementTwo = VolumnSpinnerTwo.getSelectedItemPosition();

                VolumnSpinnerOne.setSelection(measurementTwo);
                VolumnSpinnerTwo.setSelection(measurementOne);

                ConvertMeasurment();
            }
        });

        VolumnSpinnerOne.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                ConvertMeasurment();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        VolumnSpinnerTwo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                ConvertMeasurment();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        VolumnOneText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                ConvertMeasurment();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        return view;
    }

    private void ConvertMeasurment() {
        String measurmentToConvert = VolumnOneText.getText().toString();
        if(!measurmentToConvert.isEmpty()) {
            if(measurmentToConvert.equals(".")) {
                measurmentToConvert = "0.";
            }
            double valueToConvert = Double.valueOf(measurmentToConvert);
            double milliliters = ConvertVolumnToMilliliters(valueToConvert);
            VolumnTwoText.setText(String.valueOf(ConvertFromMilliliters(milliliters)));
        }
        else{
            VolumnTwoText.setText("0");
        }
    }

    private double ConvertFromMilliliters(double measurment) {
        String distanceTwoType = VolumnSpinnerTwo.getSelectedItem().toString();
        double converted = 0;

        switch (distanceTwoType) {
            case "Milliliters":
                converted = measurment * MILLILITERS;
                break;
            case "Tablespoons":
                converted = measurment * TABLESPOONS;
                break;
            case "Teaspoons":
                converted = measurment * TEASPOONS;
                break;
            case "Ounces":
                converted = measurment * OUNCES;
                break;
            case "Cups":
                converted = measurment * CUPS;
                break;
            case "Pints":
                converted = measurment * PINTS;
                break;
            case "Quarts":
                converted = measurment * QUARTS;
                break;
            case "Gallons":
                converted = measurment * GALLONS;
                break;
            case "Liters":
                converted = measurment * LITERS;
                break;
            default:
                converted = 0;
                break;
        }

        BigDecimal bd = new BigDecimal(converted);
        bd = bd.setScale(4, BigDecimal.ROUND_HALF_UP);
        converted = bd.doubleValue();
        return converted;
    }

    private double ConvertVolumnToMilliliters(double measurment) {
        String distanceOneType = VolumnSpinnerOne.getSelectedItem().toString();

        double milliliters = 0;

        switch(distanceOneType){
            case "Milliliters":
                milliliters = measurment / MILLILITERS;
                break;
            case "Tablespoons":
                milliliters = measurment / TABLESPOONS;
                break;
            case "Teaspoons":
                milliliters = measurment / TEASPOONS;
                break;
            case "Ounces":
                milliliters = measurment / OUNCES;
                break;
            case "Cups":
                milliliters = measurment / CUPS;
                break;
            case "Pints":
                milliliters = measurment / PINTS;
                break;
            case "Quarts":
                milliliters = measurment / QUARTS;
                break;
            case "Gallons":
                milliliters = measurment / GALLONS;
                break;
            case "Liters":
                milliliters = measurment / LITERS;
                break;
            default:
                milliliters = 0;
                break;
        }

        return milliliters;
    }


}
