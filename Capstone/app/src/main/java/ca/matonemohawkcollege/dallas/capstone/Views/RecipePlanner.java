package ca.matonemohawkcollege.dallas.capstone.Views;

import java.sql.Date;
import java.util.UUID;

/**
 * Created by Dallas on 1/5/2018.
 */

public class RecipePlanner {

    public RecipePlanner() {
    }

    public UUID RecipePlanner_ID;
    public UUID Recipe_ID;
    public String RecipePlanner_RecipeName;
    public Date RecipePlanner_Date;

}
