package ca.matonemohawkcollege.dallas.capstone.Dialogs;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ca.matonemohawkcollege.dallas.capstone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeNotesDialog extends DialogFragment {

    private EditText _recipeNoteText;
    private Button _saveBtn;
    private Button _cancelBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_recipe_notes_dialog, null);

        _recipeNoteText = (EditText) view.findViewById(R.id.txt_NoteText);
        _saveBtn = (Button) view.findViewById(R.id.btn_Save);
        _cancelBtn = (Button) view.findViewById(R.id.btn_Cancel);

        _saveBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Toast.makeText(getContext(), "Saved!", Toast.LENGTH_SHORT).show();
            }
        });

        _cancelBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                getDialog().dismiss();
            }
        });

        return view;
    }

}
