package ca.matonemohawkcollege.dallas.capstone.Models;

import java.util.UUID;

/**
 * Created by Dallas on 1/5/2018.
 */

public class Ingredients {

    @com.google.gson.annotations.SerializedName("id")
    public String id;

    @com.google.gson.annotations.SerializedName("recipe_ID")
    public String recipe_ID;

    @com.google.gson.annotations.SerializedName("ingredient_Name")
    public String ingredient_Name;

    public Ingredients() {

    }

    public String getId() {
        return id;
    }

    public final void setId(String text) { id = text; }


    public String getRecipeID() {
        return recipe_ID;
    }

    public final void setRecipeID(String text) {
        recipe_ID = text;
    }


    public String getIngredientsName() {
        return ingredient_Name;
    }

    public final void setIngredientsName(String text) { ingredient_Name = text; }
}
