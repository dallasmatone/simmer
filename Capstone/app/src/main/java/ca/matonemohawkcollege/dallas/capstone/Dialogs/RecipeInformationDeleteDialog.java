package ca.matonemohawkcollege.dallas.capstone.Dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import ca.matonemohawkcollege.dallas.capstone.R;

/**
 * Created by Dallas on 12/29/2017.
 */

public class RecipeInformationDeleteDialog extends DialogFragment {

    private TextView _title;
    private TextView _deleteText;
    private Button _okBtn;
    private Button _cancelBtn;

    private String _item;
    private String _type;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_recipe_information_delete_dialog, null);

        _deleteText = (TextView) view.findViewById(R.id.txt_DeleteText);
        _okBtn = (Button) view.findViewById(R.id.btn_OK);
        _cancelBtn = (Button) view.findViewById(R.id.btn_Cancel);

        _title = (TextView) view.findViewById(R.id.lbl_dialogTitle);
        _item  = getArguments().getString("item");
        _type = getArguments().getString("type");

        _title.setText("Delete " + _type);

        _deleteText.setText("Are you sure you want to delete \'" + _item + "\'");

        _okBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Toast.makeText(getContext(), "Delete " + _type, Toast.LENGTH_SHORT).show();
            }
        });

        _cancelBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                getDialog().dismiss();
            }
        });

        return view;
    }
}
