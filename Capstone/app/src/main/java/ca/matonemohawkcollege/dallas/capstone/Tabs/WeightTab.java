package ca.matonemohawkcollege.dallas.capstone.Tabs;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.math.BigDecimal;

import ca.matonemohawkcollege.dallas.capstone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class WeightTab extends Fragment {

    public Spinner WeightSpinnerOne;
    public Spinner WeightSpinnerTwo;
    public ImageButton SwapWeightsBtn;
    public EditText WeightOneText;
    public EditText WeightTwoText;

    private static final double GRAM = 1;
    private static final double MILLIGRAM = 1000;
    private static final double KILOGRAM = 0.001;
    private static final double OUNCES = 0.035274;
    private static final double POUND = 0.0022;

    public WeightTab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weight_tab, container, false);
        setHasOptionsMenu(false);

        //Initalize Public Variables
        WeightSpinnerOne = (Spinner) view.findViewById(R.id.spnWeightOne);
        WeightSpinnerTwo = (Spinner) view.findViewById(R.id.spnWeightTwo);
        SwapWeightsBtn = (ImageButton) view.findViewById(R.id.btnSwapWeights);
        WeightOneText = (EditText) view.findViewById(R.id.txtWeightOne);
        WeightTwoText = (EditText) view.findViewById(R.id.txtWeightTwo);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.array_WeightMeasurments));
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        WeightSpinnerOne.setAdapter(adapter);
        WeightSpinnerTwo.setAdapter(adapter);

        SwapWeightsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int measurementOne = WeightSpinnerOne.getSelectedItemPosition();
                int measurementTwo = WeightSpinnerTwo.getSelectedItemPosition();

                WeightSpinnerOne.setSelection(measurementTwo);
                WeightSpinnerTwo.setSelection(measurementOne);

                ConvertMeasurment();
            }
        });

        WeightSpinnerOne.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                ConvertMeasurment();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        WeightSpinnerTwo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                ConvertMeasurment();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        WeightOneText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                ConvertMeasurment();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        return view;
    }

    private void ConvertMeasurment() {
        String measurmentToConvert = WeightOneText.getText().toString();
        if(!measurmentToConvert.isEmpty()) {
            if(measurmentToConvert.equals(".")) {
                measurmentToConvert = "0.";
            }
            double valueToConvert = Double.valueOf(measurmentToConvert);
            double grams = ConvertWeightToGrams(valueToConvert);
            WeightTwoText.setText(String.valueOf(ConvertFromGrams(grams)));
        }
        else{
            WeightTwoText.setText("0");
        }
    }

    private double ConvertFromGrams(double measurment) {
        String distanceTwoType = WeightSpinnerTwo.getSelectedItem().toString();
        double converted = 0;

        switch (distanceTwoType) {
            case "Grams":
                converted = measurment * GRAM;
                break;
            case "Milligrams":
                converted = measurment * MILLIGRAM;
                break;
            case "Kilograms":
                converted = measurment * KILOGRAM;
                break;
            case "Ounces":
                converted = measurment * OUNCES;
                break;
            case "Pounds":
                converted = measurment * POUND;
                break;
            default:
                converted = 0;
                break;
        }

        BigDecimal bd = new BigDecimal(converted);
        bd = bd.setScale(4, BigDecimal.ROUND_HALF_UP);
        converted = bd.doubleValue();
        return converted;
    }

    private double ConvertWeightToGrams(double measurment) {
        String distanceOneType = WeightSpinnerOne.getSelectedItem().toString();

        double grams = 0;

        switch(distanceOneType){
            case "Grams":
                grams = measurment / GRAM;
                break;
            case "Milligrams":
                grams = measurment / MILLIGRAM;
                break;
            case "Kilograms":
                grams = measurment / KILOGRAM;
                break;
            case "Ounces":
                grams = measurment / OUNCES;
                break;
            case "Pounds":
                grams = measurment / POUND;
                break;
        }

        return grams;
    }

}
