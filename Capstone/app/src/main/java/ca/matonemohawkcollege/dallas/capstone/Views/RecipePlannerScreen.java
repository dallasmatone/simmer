package ca.matonemohawkcollege.dallas.capstone.Views;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ImageButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ca.matonemohawkcollege.dallas.capstone.Dialogs.AddEditRecipePlanDialog;
import ca.matonemohawkcollege.dallas.capstone.Dialogs.RecipeInformationDeleteDialog;
import ca.matonemohawkcollege.dallas.capstone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipePlannerScreen extends Fragment {

    private ImageButton _addRecipePlan;
    private ImageButton _editRecipePlan;
    private ImageButton _deleteRecipePlan;
    private ImageButton _gotoRecipePlan;
    private CalendarView _recipePlanner;
    private FragmentManager _fragmentManager;
    private SimpleDateFormat _dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private int _selectedYear;
    private int _selectedMonth;
    private int _selectedDay;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        getActivity().setTitle("Recipe Planner");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recipe_planner_screen, container, false);
        _recipePlanner = (CalendarView) view.findViewById(R.id.calendarView);
        _addRecipePlan = (ImageButton) view.findViewById(R.id.btn_addRecipePlan);
        _editRecipePlan = (ImageButton) view.findViewById(R.id.btn_editRecipePlan);
        _deleteRecipePlan = (ImageButton) view.findViewById(R.id.btn_deleteRecipePlan);
        _gotoRecipePlan = (ImageButton) view.findViewById(R.id.btn_gotoRecipePlan);
        _fragmentManager = getFragmentManager();
        LoadData();

        _recipePlanner.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int day) {
                _selectedYear = year;
                _selectedMonth = month;
                _selectedDay = day;
                CheckIfDateIsPast(year, month, day);
            }
        });

        _addRecipePlan.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("year", _selectedYear);
                bundle.putInt("month", _selectedMonth);
                bundle.putInt("day", _selectedDay);
                bundle.putString("type", "Add");
                AddEditRecipePlanDialog addEditRecipePlanDialog = new AddEditRecipePlanDialog();
                addEditRecipePlanDialog.setArguments(bundle);
                addEditRecipePlanDialog.show(_fragmentManager, "AddEditRecipePlanDialog");
            }
        });

        _editRecipePlan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("year", _selectedYear);
                bundle.putInt("month", _selectedMonth);
                bundle.putInt("day", _selectedDay);
                bundle.putString("type", "Edit");
                AddEditRecipePlanDialog addEditRecipePlanDialog = new AddEditRecipePlanDialog();
                addEditRecipePlanDialog.setArguments(bundle);
                addEditRecipePlanDialog.show(_fragmentManager, "AddEditRecipePlanDialog");
            }
        });

        _deleteRecipePlan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("item", "Recipe");
                bundle.putString("type", "Recipe Plan");
                RecipeInformationDeleteDialog deleteDialog = new RecipeInformationDeleteDialog();
                deleteDialog.setArguments(bundle);
                deleteDialog.show(_fragmentManager, "RecipeInformationDeleteDialog");
            }
        });

        _gotoRecipePlan.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Fragment fragment = new RecipeScreen();
                Bundle bundle = new Bundle();
                bundle.putString("recipeName", "CALENDER RECIPE");
                bundle.putString("CRUD", "View");
                fragment.setArguments(bundle);

                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.content_container, fragment);
                fragmentTransaction.addToBackStack("RecipeSearch");
                fragmentTransaction.commit();
                setHasOptionsMenu(false);
            }
        });

        return view;
    }

    private void LoadData() {
        Calendar calendar = Calendar.getInstance();
        _selectedYear = calendar.get(Calendar.YEAR);
        _selectedMonth = calendar.get(Calendar.MONTH);
        _selectedDay = calendar.get(Calendar.DAY_OF_MONTH);
    }

    private void CheckIfDateIsPast(int year, int month, int day){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date today = c.getTime();

        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);

        Date dateSpecified = c.getTime();

        if (!dateSpecified.before(today)) {
            _addRecipePlan.setEnabled(true);
            _addRecipePlan.setImageResource(R.drawable.icons8_plus);
        } else {
            _addRecipePlan.setEnabled(false);
            _addRecipePlan.setImageResource(R.drawable.icons8_plus_disabled);
        }
    }

    public String getCurrentDate() {
        Date date = new Date();
        return _dateFormat.format(date);
    }

}
