package ca.matonemohawkcollege.dallas.capstone.Dialogs;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ca.matonemohawkcollege.dallas.capstone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeAddSaveDialog extends DialogFragment {

    private TextView _titleLbl;
    private EditText _informationText;
    private Button _addSaveBtn;
    private Button _cancelBtn;

    private String _title;
    private String _ingredient;
    private String _type;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_ingrediant_dialog, null);
        _titleLbl = (TextView) view.findViewById(R.id.lbl_dialogTitle);
        _informationText = (EditText) view.findViewById(R.id.txt_InfoName);
        _addSaveBtn = (Button) view.findViewById(R.id.btn_AddSaveInfo);
        _cancelBtn = (Button) view.findViewById(R.id.btn_Cancel);

        _addSaveBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Toast.makeText(getContext(), _title + " Item", Toast.LENGTH_SHORT).show();
            }
        });

        _cancelBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                getDialog().dismiss();
            }
        });

        _title  = getArguments().getString("title");
        _ingredient = getArguments().getString("item");
        _type = getArguments().getString("type");

        _titleLbl.setText(_title + " " + _type);
        if(_title.equals("Edit")) {
            _informationText.setText(_ingredient);
            _addSaveBtn.setText("Save");
        }else{
            _addSaveBtn.setText(_title);
        }

        return view;
    }
}
