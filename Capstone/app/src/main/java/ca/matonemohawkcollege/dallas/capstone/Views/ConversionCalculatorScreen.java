package ca.matonemohawkcollege.dallas.capstone.Views;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabWidget;

import ca.matonemohawkcollege.dallas.capstone.Tabs.DistanceTab;
import ca.matonemohawkcollege.dallas.capstone.R;
import ca.matonemohawkcollege.dallas.capstone.Tabs.TempuratureTab;
import ca.matonemohawkcollege.dallas.capstone.Tabs.VolumnTab;
import ca.matonemohawkcollege.dallas.capstone.Tabs.WeightTab;


/**
 * A simple {@link Fragment} subclass.
 * This Screen allows a user to look convert seevral different values
 */
public class ConversionCalculatorScreen extends Fragment {

    //Declare Public Variables
    private FragmentTabHost mTabHost;

    public ConversionCalculatorScreen() {
        // Required empty public constructor
    }

    /***
     * Creates the conversion view
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Conversion Calculator");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mTabHost = new FragmentTabHost(getActivity());
        mTabHost.setup(getActivity(), getChildFragmentManager(), R.layout.fragment_conversion_calculator_screen);

        //Tab 1
        InitalizeDistanceTab();
        //Tab 2
        InitalizeTempuratureTab();
        //Tab 3
        InitalizeVolumnTab();
        //Tab 4
        InitalizeWeightTab();

        for(int i = 0; i < 4; i++)
            mTabHost.getTabWidget().getChildAt(i).setBackgroundColor(getResources().getColor(R.color.colorMain));

        View tabs = (TabWidget) mTabHost.findViewById(android.R.id.tabs);
        ViewGroup parent = (ViewGroup) mTabHost.getChildAt(0);
        parent.removeView(tabs);
        parent.addView(tabs);

        return mTabHost;
    }

    private void InitalizeDistanceTab(){
        TabHost.TabSpec spec = mTabHost.newTabSpec("Tab One");
        spec.setIndicator("",getResources().getDrawable(R.drawable.icons8_ruler));
        mTabHost.addTab(spec, DistanceTab.class, null);
    }

    private void InitalizeTempuratureTab(){
        TabHost.TabSpec spec = mTabHost.newTabSpec("Tab Two");
        spec.setIndicator("",getResources().getDrawable(R.drawable.icons8_temperature));
        mTabHost.addTab(spec, TempuratureTab.class, null);
    }

    private void InitalizeVolumnTab(){
        TabHost.TabSpec spec = mTabHost.newTabSpec("Tab Three");
        spec.setIndicator("",getResources().getDrawable(R.drawable.icons8_volume));
        mTabHost.addTab(spec, VolumnTab.class, null);
    }

    private void InitalizeWeightTab(){
        TabHost.TabSpec spec = mTabHost.newTabSpec("Tab Four");
        spec.setIndicator("",getResources().getDrawable(R.drawable.icons8_scales));
        mTabHost.addTab(spec, WeightTab.class, null);
    }

}
