package ca.matonemohawkcollege.dallas.capstone.Base;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import ca.matonemohawkcollege.dallas.capstone.Models.Users;
import ca.matonemohawkcollege.dallas.capstone.ODataWrappers.ODataWrapperRecipePlanner;

/**
 * Created by Dallas on 1/5/2018.
 */

public class RecipePlannerBase extends AsyncTask<String, Void, String> {

    private Activity myActivity;
    public ODataWrapperRecipePlanner RecipePlannerList;

    public RecipePlannerBase(Activity inActivity) {
        myActivity = inActivity;
    }

    /**
     * Starts to download the list of information based on the url
     * @param params the url to be searched
     * @return a list of science fair projects
     */
    @Override
    protected String doInBackground(String... params) {
        String results = "";

        try {
            URL url = new URL(params[0].replace(" ", "%20"));// replaces all spaces with URL friendly %20 that act as spaces in URLs

            // Open the Connection - GET is the default setRequestMethod
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // Read the response
            int statusCode = conn.getResponseCode();

            if (statusCode == 200) {
                InputStream inputStream = new BufferedInputStream(conn.getInputStream());

                BufferedReader bufferedReader =
                        new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    results += line;                // Use a StringBuilder class if expecting many lines!!
                }
            }

        } catch (IOException ex) {
        }

        return results;
    }

    /**
     * Once the download is done, assign all the information to an array list. If something is clicked
     * in the list, send an intent of the information to a different activity.
     * @param result the list of science fair projects
     */
    protected void onPostExecute(String result) {

        Gson gson = new Gson();
        Users users = new Users();
        JSONObject JObj= null;
        try {
            JObj = new JSONObject(result);
            JSONArray array_list = JObj.getJSONArray("value");

            RecipePlannerList = gson.fromJson(array_list.toString(), ODataWrapperRecipePlanner.class);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}