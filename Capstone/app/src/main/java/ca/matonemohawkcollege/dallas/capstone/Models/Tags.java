package ca.matonemohawkcollege.dallas.capstone.Models;

import java.util.UUID;

/**
 * Created by Dallas on 1/5/2018.
 */

public class Tags {

    @com.google.gson.annotations.SerializedName("id")
    public String id;

    @com.google.gson.annotations.SerializedName("recipe_ID")
    public String recipe_ID;

    @com.google.gson.annotations.SerializedName("tags_IsVegan")
    public boolean tag_IsVegan;

    @com.google.gson.annotations.SerializedName("tags_IsVegetarian")
    public boolean tag_IsVegetarian;

    @com.google.gson.annotations.SerializedName("tags_IsHalal")
    public boolean tag_IsHalal;

    @com.google.gson.annotations.SerializedName("tags_IsKosher")
    public boolean tag_IsKosher;

    @com.google.gson.annotations.SerializedName("tags_IsDairyFree")
    public boolean tag_IsDairyFree;

    @com.google.gson.annotations.SerializedName("tags_IsLowFat")
    public boolean tag_IsLowFat;

    public Tags() {

    }

    public String getId() {
        return id;
    }

    public final void setId(String text) { id = text; }


    public String getRecipeID() {
        return recipe_ID;
    }

    public final void setRecipeID(String text) {
        recipe_ID = text;
    }

    public boolean getTagIsVegan(){
        return tag_IsVegan;
    }

    public final void setTagIsVegan(boolean value){
        tag_IsVegan = value;
    }

    public boolean getTagIsVegetarian(){
        return tag_IsVegetarian;
    }

    public final void setTagIsVegetarian(boolean value){
        tag_IsVegetarian = value;
    }

    public boolean getTagIsHalal(){
        return tag_IsHalal;
    }

    public final void setTagIsHalal(boolean value){
        tag_IsHalal = value;
    }

    public boolean getTagIsKosher(){
        return tag_IsKosher;
    }

    public final void setTagIsKosher(boolean value){
        tag_IsKosher = value;
    }

    public boolean getTagIsDairyFree(){
        return tag_IsDairyFree;
    }

    public final void setTagIsDairyFree(boolean value){
        tag_IsDairyFree = value;
    }

    public boolean getTagIsLowFat(){ return tag_IsLowFat; }

    public final void setTagIsLowFat(boolean value){
        tag_IsLowFat = value;
    }

}
