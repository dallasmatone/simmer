package ca.matonemohawkcollege.dallas.capstone.Dialogs;

import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_NAME_COLUMN;
import static ca.matonemohawkcollege.dallas.capstone.ColumnConstants.RECIPE_ID;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.support.v7.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.FragmentManager;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import ca.matonemohawkcollege.dallas.capstone.DatePickerFragment;
import ca.matonemohawkcollege.dallas.capstone.NonScrollListView;
import ca.matonemohawkcollege.dallas.capstone.R;
import ca.matonemohawkcollege.dallas.capstone.RecipeListViewAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddEditRecipePlanDialog extends DialogFragment  {

    private FragmentManager _fragmentManager;

    private SearchView _searchView;
    private NonScrollListView _recipeListView;
    private TextView _dateTxt;
    private ImageButton _datePicker;
    private Button _saveBtn;
    private Button _cancelBtn;
    private TextView _title;

    private ArrayList<HashMap<String, String>> _recipeList;
    private String _selectedRecipe;
    private String _selectedRecipeID;
    private RecipeListViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_add_edit_recipe_plan_dialog, null);

        _fragmentManager = getFragmentManager();
        _searchView = (SearchView) view.findViewById(R.id.sv_RecipeSearch);
        _recipeListView= (NonScrollListView) view.findViewById(R.id.lv_RecipeList);
        _dateTxt = (TextView) view.findViewById(R.id.txt_Date);
        _datePicker = (ImageButton) view.findViewById(R.id.btn_DatePicker);
        _saveBtn = (Button) view.findViewById(R.id.btn_Save);
        _cancelBtn = (Button) view.findViewById(R.id.btn_Cancel);
        _title = (TextView) view.findViewById(R.id.lbl_dialogTitle);
        _recipeList = new ArrayList<>();

        LoadData();

        _searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                ArrayList<HashMap<String, String>> tempList = new ArrayList<>();


                for (HashMap<String, String> temp : _recipeList){
                    if(temp.get(RECIPE_NAME_COLUMN).toLowerCase().contains(s.toLowerCase())){
                        tempList.add(temp);
                    }
                }
                adapter = new RecipeListViewAdapter(getActivity(), tempList);
                _recipeListView.setAdapter(adapter);

                return true;
            }
        });

        _recipeListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id)
            {
                HashMap<String, String> temp  = (HashMap<String, String>) adapter.getItem(position);
                _selectedRecipeID = temp.get(RECIPE_ID);
                _selectedRecipe = temp.get(RECIPE_NAME_COLUMN);
            }

        });

        _datePicker.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                datePicker(view);
            }
        });

        _saveBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Toast.makeText(getContext(), _selectedRecipe, Toast.LENGTH_SHORT).show();
            }
        });

        _cancelBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                getDialog().dismiss();
            }
        });

        return view;
    }

    private void LoadData(){
        for(int i = 0; i < 20; i++){
            HashMap<String,String> temp=new HashMap<String, String>();
            temp.put(RECIPE_NAME_COLUMN, String.valueOf(i));
            temp.put(RECIPE_ID, String.valueOf(i));
            _recipeList.add(temp);
        }
        adapter =  new RecipeListViewAdapter(getActivity(), _recipeList);
        _recipeListView.setAdapter(adapter);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, getArguments().getInt("year"));
        calendar.set(Calendar.MONTH, getArguments().getInt("month"));
        calendar.set(Calendar.DAY_OF_MONTH, getArguments().getInt("day"));
        setDate(calendar);
        String type = getArguments().getString("type");

        _title.setText(type + " Recipe Plan");
        if(type.equals("Add"))
            _saveBtn.setText("Create");
        else
            _saveBtn.setText("Save");
    }

    private void datePicker(View view){
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }

    private void setDate(final Calendar calendar) {
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        _dateTxt.setText(dateFormat.format(calendar.getTime()));
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int month,
                              int day) {

            Calendar cal = new GregorianCalendar(year, month, day);
            setDate(cal);
        }
    };
}
