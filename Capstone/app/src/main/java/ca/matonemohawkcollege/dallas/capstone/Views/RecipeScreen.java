package ca.matonemohawkcollege.dallas.capstone.Views;


import android.support.v4.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.util.ArrayList;

import at.markushi.ui.CircleButton;
import ca.matonemohawkcollege.dallas.capstone.Dialogs.RecipeAddSaveDialog;
import ca.matonemohawkcollege.dallas.capstone.Dialogs.RecipeInformationDeleteDialog;
import ca.matonemohawkcollege.dallas.capstone.Dialogs.RecipeNotesDialog;
import ca.matonemohawkcollege.dallas.capstone.Dialogs.RecipeTagsDialog;
import ca.matonemohawkcollege.dallas.capstone.Dialogs.RecipeTypeDialog;
import ca.matonemohawkcollege.dallas.capstone.Managers.RecipesManager;
import ca.matonemohawkcollege.dallas.capstone.Models.Recipes;
import ca.matonemohawkcollege.dallas.capstone.NonScrollListView;
import ca.matonemohawkcollege.dallas.capstone.R;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeScreen extends Fragment {

    private FragmentManager _fragmentManager;
    private RecipesManager _recipeManager;

    private CircleButton _editRecipePicBtn;
    private ImageView _recipePicImg;
    private ImageButton _recipeEditBtn;
    private TextView _recipeNameText;
    private RatingBar _recipeRating;
    private ImageButton _recipeNotesBtn;
    private EditText _recipeDescription;
    private NonScrollListView _recipeIngredients;
    private NonScrollListView _recipeSupplies;
    private NonScrollListView _recipeSteps;
    private ImageButton _recipeDelete;
    private ImageButton _recipeFavoruite;
    private ViewSwitcher _switcher;
    private EditText _recipeNameEditText;
    private ImageButton _recipeTagsBtn;
    private ImageButton _recipeType;

    //Data Holders for lists
    private ArrayList<String> _recipeIngredientsData = new ArrayList<String>();
    private ArrayList<String> _recipeSuppliesData = new ArrayList<String>();
    private ArrayList<String> _recipeStepsData = new ArrayList<String>();
    private ArrayAdapter<String> _adapterIngrediants;
    private ArrayAdapter<String> _adapterSupplies;
    private ArrayAdapter<String> _adapterSteps;

    //Crud Buttons
    private ImageButton _addRecipeIngredientsBtn;
    private ImageButton _editRecipeIngredientsBtn;
    private ImageButton _deleteRecipeIngredientsBtn;
    private ImageButton _addRecipeSuppliesBtn;
    private ImageButton _editRecipeSuppliesBtn;
    private ImageButton _deleteRecipeSuppliesBtn;
    private ImageButton _addRecipeStepsBtn;
    private ImageButton _editRecipeStepsBtn;
    private ImageButton _deleteRecipeStepsBtn;

    private Boolean isEditMode = false;
    private Boolean isFavourite = false;
    private String _selectedIngrediant;
    private String _selectedSupply;
    private String _selectedStep;
    private String _recipeName;
    private String _crudType;
    private String _userID;
    private String _recipeID;

    private Recipes _recipe;

    public RecipeScreen() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recipe_screen, container, false);
        _recipeManager = new RecipesManager(getActivity());
        getActivity().setTitle("Recipe");
        setHasOptionsMenu(false);
        _recipe = new Recipes();
        _fragmentManager = getFragmentManager();
        _crudType = getArguments().getString("CRUD");
        InitalizeProperties(view);
        InitalizeView(view);
        DisableAllEditables();

        _recipeIngredients.setEmptyView(view.findViewById(R.id.emptyIngredients));
        _recipeSupplies.setEmptyView(view.findViewById(R.id.emptySupplies));
        _recipeSteps.setEmptyView(view.findViewById(R.id.emptySteps));

        LoadData(view);


        _editRecipePicBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                alertDialog();
            }
        });

        _recipeEditBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
               OnEdit();
            }
        });

        _recipeFavoruite.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(!isFavourite) {
                    isFavourite = true;
                    _recipeFavoruite.setImageResource(R.drawable.icons8_heart);
                }
                else{
                    isFavourite = false;
                    _recipeFavoruite.setImageResource(R.drawable.icons8_heart_empty);
                }
            }
        });

        _recipeNotesBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                RecipeNotesDialog recipeNotesDialog = new RecipeNotesDialog();
                recipeNotesDialog.show(_fragmentManager, "RecipeNotesDialog");
            }
        });

        _recipeDelete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                OnDelete(_recipeNameText.getText().toString(), "Recipe");
            }
        });

        _recipeTagsBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                RecipeTagsDialog recipeTagsDialog = new RecipeTagsDialog();
                recipeTagsDialog.show(_fragmentManager, "RecipeTagsDialog");
            }
        });

        _recipeType.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                RecipeTypeDialog recipeTypeDialog = new RecipeTypeDialog();
                recipeTypeDialog.show(_fragmentManager, "RecipeTypeDialog");
            }
        });

        _recipeIngredients.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                _selectedIngrediant = (String) _recipeIngredients.getItemAtPosition(position);
                _editRecipeIngredientsBtn.setImageResource(R.drawable.icons8_edit);
                _editRecipeIngredientsBtn.setEnabled(true);
                _deleteRecipeIngredientsBtn.setImageResource(R.drawable.icons8_trash);
                _deleteRecipeIngredientsBtn.setEnabled(true);
            }
        });

        _recipeSupplies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                _selectedSupply = (String) _recipeSupplies.getItemAtPosition(position);
                _editRecipeSuppliesBtn.setImageResource(R.drawable.icons8_edit);
                _editRecipeSuppliesBtn.setEnabled(true);
                _deleteRecipeSuppliesBtn.setImageResource(R.drawable.icons8_trash);
                _deleteRecipeSuppliesBtn.setEnabled(true);
            }
        });

        _recipeSteps.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                _selectedStep = (String) _recipeSteps.getItemAtPosition(position);
                _editRecipeStepsBtn.setImageResource(R.drawable.icons8_edit);
                _editRecipeStepsBtn.setEnabled(true);
                _deleteRecipeStepsBtn.setImageResource(R.drawable.icons8_trash);
                _deleteRecipeStepsBtn.setEnabled(true);
            }
        });

        _addRecipeIngredientsBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                OnAddSave("Add", "Ingredient", null);
            }
        });

        _editRecipeIngredientsBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                OnAddSave("Edit", "Ingredient", _selectedIngrediant);
            }
        });

        _deleteRecipeIngredientsBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                OnDelete(_selectedIngrediant, "Ingredient");
            }
        });

        _addRecipeSuppliesBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                OnAddSave("Add", "Supply", null);
            }
        });

        _editRecipeSuppliesBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                OnAddSave("Edit", "Supply", _selectedSupply);
            }
        });

        _deleteRecipeSuppliesBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                OnDelete(_selectedSupply, "Supply");
            }
        });

        _addRecipeStepsBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                OnAddSave("Add", "Step", null);
            }
        });

        _editRecipeStepsBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                OnAddSave("Edit", "Step", _selectedStep);
            }
        });

        _deleteRecipeStepsBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                OnDelete(_selectedStep, "Step");
            }
        });

        return view;
    }

    private void InitalizeView(View view) {
        _recipeNameText.setLongClickable(false);
        _recipeDescription.setLongClickable(false);
    }

    private void OnDelete(String item, String type){
        Bundle bundle = new Bundle();
        bundle.putString("item", item);
        bundle.putString("type", type);
        RecipeInformationDeleteDialog deleteDialog = new RecipeInformationDeleteDialog();
        deleteDialog.setArguments(bundle);
        deleteDialog.show(_fragmentManager, "RecipeInformationDeleteDialog");
    }

    private void OnAddSave(String title, String type, String item){
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("type", type);
        if(item != null)
            bundle.putString("item", item);
        RecipeAddSaveDialog recipeAddSaveDialog = new RecipeAddSaveDialog();
        recipeAddSaveDialog.setArguments(bundle);
        recipeAddSaveDialog.show(_fragmentManager, "RecipeAddSaveDialog");
    }

    private void LoadData(View view) {
        if(_crudType.equals("View")) {
            GetListOfIngredients();
            _adapterIngrediants = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_list_item_1, android.R.id.text1, _recipeIngredientsData);
            _adapterSupplies = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_list_item_1, android.R.id.text1, _recipeSuppliesData);
            _adapterSteps = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_list_item_1, android.R.id.text1, _recipeStepsData);

            _recipeIngredients.setAdapter(_adapterSteps);
            _recipeSupplies.setAdapter(_adapterIngrediants);
            _recipeSteps.setAdapter(_adapterSupplies);
        }else{

        }
        Float rating;
        String userRating = getArguments().getString("recipeRating");
        if(userRating == null || userRating.isEmpty())
            rating = 0f;
        else
            rating = Float.valueOf(userRating);

        _recipe.setUserID(getArguments().getString("UserID"));
        _recipe.setId(getArguments().getString("ID"));
        _recipe.setRecipeDescription(getArguments().getString("recipeDescription"));
        _recipe.setIsFavorutie(Boolean.valueOf(getArguments().getString("recipeFav")));
        _recipe.setRecipeRating(getArguments().getFloat("recipeRating"));
        _recipe.setRecipeName(getArguments().getString("recipeName"));
        _recipe.setRecipeID(getArguments().getString("recipeID"));

        if(_recipe != null)
            _recipeNameText.setText(_recipe.getRecipeName());

        if(_recipe != null)
            _recipeDescription.setText(_recipe.getRecipeDescription());
        _recipeRating.setRating(_recipe.getRecipeRating());

        isFavourite = _recipe.getIsFavorutie();
        if(isFavourite) {
            _recipeFavoruite.setImageResource(R.drawable.icons8_heart);
        }
        else{
            _recipeFavoruite.setImageResource(R.drawable.icons8_heart_empty);
        }
    }

    private void InitalizeProperties(View view){
        _editRecipePicBtn = (CircleButton) view.findViewById(R.id.recipePicBtn);
        _recipePicImg = (ImageView) view.findViewById(R.id.recipeImg);
        _recipeNameText = (TextView) view.findViewById(R.id.lbl_recipeName);
        _recipeEditBtn = (ImageButton) view.findViewById(R.id.editRecipeBtn);
        _recipeRating = (RatingBar) view.findViewById(R.id.rtbRecipeRating);
        _recipeNotesBtn = (ImageButton) view.findViewById(R.id.recipeNotesBtn);
        _recipeDescription = (EditText) view.findViewById(R.id.txt_recipeDescription);
        _recipeIngredients = (NonScrollListView) view.findViewById(R.id.lv_RecipeIngrediants);
        _recipeSupplies = (NonScrollListView) view.findViewById(R.id.lv_RecipeSupplies);
        _recipeSteps = (NonScrollListView) view.findViewById(R.id.lv_RecipeSteps);
        _recipeDelete = (ImageButton) view.findViewById(R.id.deleteRecipeBtn);
        _recipeFavoruite = (ImageButton) view.findViewById(R.id.favoruiteRecipeBtn);
        _switcher = (ViewSwitcher) view.findViewById(R.id.my_switcher);
        _recipeNameEditText = (EditText) _switcher.findViewById(R.id.txt_recipeName);
        _recipeTagsBtn = (ImageButton) view.findViewById(R.id.btn_tags);
        _recipeType = (ImageButton) view.findViewById(R.id.btn_RecipeType);

        //CRUD Buttons
        _addRecipeIngredientsBtn = (ImageButton) view.findViewById(R.id.btn_addIngrediant);
        _editRecipeIngredientsBtn = (ImageButton) view.findViewById(R.id.btn_editIngrediant);
        _deleteRecipeIngredientsBtn = (ImageButton) view.findViewById(R.id.btn_deleteIngrediant);
        _addRecipeSuppliesBtn = (ImageButton) view.findViewById(R.id.btn_addSupply);
        _editRecipeSuppliesBtn = (ImageButton) view.findViewById(R.id.btn_editSupply);
        _deleteRecipeSuppliesBtn = (ImageButton) view.findViewById(R.id.btn_deleteSupply);
        _addRecipeStepsBtn = (ImageButton) view.findViewById(R.id.btn_addStep);
        _editRecipeStepsBtn = (ImageButton) view.findViewById(R.id.btn_editStep);
        _deleteRecipeStepsBtn = (ImageButton) view.findViewById(R.id.btn_deleteStep);
    }

    private void GetListOfIngredients() {
        for (int i = 0; i < 3; i++){
            _recipeIngredientsData.add("This is row Number: " + i );
            _recipeSuppliesData.add("This is row Number: " + i );
            _recipeStepsData.add("This is row Number: " + i );
        }
    }

    private void DisableAllEditables(){
        _recipeDescription.setFocusableInTouchMode(isEditMode);
        _recipeDescription.setFocusable(isEditMode);
        _recipeRating.setFocusable(isEditMode);
        _recipeRating.setIsIndicator(!isEditMode);
        _editRecipePicBtn.setEnabled(isEditMode);

        _editRecipeIngredientsBtn.setImageResource(R.drawable.icons8_edit_disabled);
        _editRecipeIngredientsBtn.setEnabled(false);
        _deleteRecipeIngredientsBtn.setImageResource(R.drawable.icons8_trash_disabled);
        _deleteRecipeIngredientsBtn.setEnabled(false);

        _editRecipeSuppliesBtn.setImageResource(R.drawable.icons8_edit_disabled);
        _editRecipeSuppliesBtn.setEnabled(false);
        _deleteRecipeSuppliesBtn.setImageResource(R.drawable.icons8_trash_disabled);
        _deleteRecipeSuppliesBtn.setEnabled(false);

        _editRecipeStepsBtn.setImageResource(R.drawable.icons8_edit_disabled);
        _editRecipeStepsBtn.setEnabled(false);
        _deleteRecipeStepsBtn.setImageResource(R.drawable.icons8_trash_disabled);
        _deleteRecipeStepsBtn.setEnabled(false);
    }

    private void OnEdit(){
        if(!isEditMode) {
            _recipeEditBtn.setImageResource(R.drawable.icons8_save);
            isEditMode = true;
            _addRecipeIngredientsBtn.setVisibility(View.VISIBLE);
            _editRecipeIngredientsBtn.setVisibility(View.VISIBLE);
            _deleteRecipeIngredientsBtn.setVisibility(View.VISIBLE);
            _addRecipeSuppliesBtn.setVisibility(View.VISIBLE);
            _editRecipeSuppliesBtn.setVisibility(View.VISIBLE);
            _deleteRecipeSuppliesBtn.setVisibility(View.VISIBLE);
            _addRecipeStepsBtn.setVisibility(View.VISIBLE);
            _editRecipeStepsBtn.setVisibility(View.VISIBLE);
            _deleteRecipeStepsBtn.setVisibility(View.VISIBLE);
            _recipeName = _recipeNameText.getText().toString();
            _switcher.showNext();
            _recipeNameEditText.setText(_recipeName);
            _recipeNameText.setLongClickable(true);
            _recipeDescription.setLongClickable(true);
        }
        else{
            isEditMode = false;
            _recipeEditBtn.setImageResource(R.drawable.icons8_edit);
            _addRecipeIngredientsBtn.setVisibility(View.GONE);
            _editRecipeIngredientsBtn.setVisibility(View.GONE);
            _deleteRecipeIngredientsBtn.setVisibility(View.GONE);
            _addRecipeSuppliesBtn.setVisibility(View.GONE);
            _editRecipeSuppliesBtn.setVisibility(View.GONE);
            _deleteRecipeSuppliesBtn.setVisibility(View.GONE);
            _addRecipeStepsBtn.setVisibility(View.GONE);
            _editRecipeStepsBtn.setVisibility(View.GONE);
            _deleteRecipeStepsBtn.setVisibility(View.GONE);
            _recipeName = _recipeNameEditText.getText().toString();
            _switcher.showPrevious();
            _recipeNameText.setText(_recipeName);
            _recipeNameText.setLongClickable(false);
            _recipeDescription.setLongClickable(false);

            _recipe.setRecipeName(_recipeName);
            _recipe.setRecipeDescription(_recipeDescription.getText().toString());
            _recipe.setRecipeRating(_recipeRating.getRating());
            _recipe.setIsFavorutie(isFavourite);

            Recipes recipe = new Recipes();
            recipe.setId(_recipe.getId());
            recipe.setRecipeID(_recipe.getRecipeID());
            recipe.setUserID(_recipe.getUserID());
            recipe.setRecipeName(_recipe.getRecipeName());
            recipe.setRecipeDescription(_recipe.getRecipeDescription());
            recipe.setRecipeRating(_recipe.getRecipeRating());
            recipe.setIsFavorutie(_recipe.getIsFavorutie());
            _recipeManager.SaveRecipe(recipe);
        }

        _recipeDescription.setFocusableInTouchMode(isEditMode);
        _recipeDescription.setFocusable(isEditMode);
        _recipeRating.setFocusable(isEditMode);
        _recipeRating.setIsIndicator(!isEditMode);
        _editRecipePicBtn.setEnabled(isEditMode);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case 0:
                if(resultCode == RESULT_OK){
                    Bitmap bitmap = (Bitmap)data.getExtras().get("data");
                    _recipePicImg.setImageBitmap(bitmap);
                }
                break;
            case 1:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = data.getData();
                    _recipePicImg.setImageURI(selectedImage);
                }
                break;
        }
    }

    private void alertDialog(){
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent,0);
                } else if (items[item].equals("Choose from Library")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);//one can be replaced with any action code
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

}
