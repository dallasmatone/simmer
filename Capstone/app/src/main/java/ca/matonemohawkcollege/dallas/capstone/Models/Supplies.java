package ca.matonemohawkcollege.dallas.capstone.Models;

/**
 * Created by Dallas on 1/5/2018.
 */

public class Supplies {
    @com.google.gson.annotations.SerializedName("id")
    public String id;

    @com.google.gson.annotations.SerializedName("recipe_ID")
    public String recipe_ID;

    @com.google.gson.annotations.SerializedName("supply_Name")
    public String supply_Name;

    public Supplies() {

    }

    public String getId() {
        return id;
    }

    public final void setId(String text) { id = text; }


    public String getRecipeID() {
        return recipe_ID;
    }

    public final void setRecipeID(String text) {
        recipe_ID = text;
    }


    public String getSupplyName() {
        return supply_Name;
    }

    public final void setSupplyName(String text) { supply_Name = text; }
}
